package com.example.yuri.toolbarresearch;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.database.MatrixCursor;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.ShareActionProvider;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import java.util.Arrays;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "MainActivity";
    private TextView tvResult;
    private List<String> items;
    private Menu menu;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        String[] arr = {"a","b","c","d","e","aa","aaa"};
        items = Arrays.asList(arr);

        tvResult = (TextView) findViewById(R.id.searchViewResult);
    }

    /*private void setupSearchView() {
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        searchView = (SearchView) findViewById(R.id.searchView);
        SearchableInfo searchableInfo = searchManager.getSearchableInfo(getComponentName());
        searchView.setSearchableInfo(searchableInfo);
    }*/

    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        getMenuInflater().inflate(R.menu.active_menu, menu);
        this.menu = menu;



        // Associate searchable configuration with the SearchView
        SearchManager searchManager =
                (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView =
                (SearchView) menu.findItem(R.id.action_search).getActionView();
        searchView.setSearchableInfo(
                searchManager.getSearchableInfo(getComponentName()));

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                //getSupportActionBar().invalidateOptionsMenu();
                //invalidateOptionsMenu();
                onPrepareOptionsMenu(menu);
                loadHistory(newText);
                return true;
            }
        });


        /*MenuItem searchItem = menu.findItem(R.id.action_search);
        searchView = (SearchView) MenuItemCompat.getActionView(searchItem);
        // Expand the search view and request focus
        searchView.onActionViewExpanded();
        searchView.requestFocus();*/

        return true;
    }

    private void loadHistory(String query) {
            // Cursor
            String[] columns = new String[] { "_id", "text" };
            Object[] temp = new Object[] { 0, "default" };

            MatrixCursor cursor = new MatrixCursor(columns);

            int pos = 0;
            for (String str: items){
                if (TextUtils.isEmpty(query) || !str.contains(query)) continue;
                temp[0] = pos;
                temp[1] = str;
                pos++;
                cursor.addRow(temp);
            }


            // SearchView
            //SearchManager manager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);

            SearchView search = (SearchView) menu.findItem(R.id.action_search).getActionView();
            search.setSuggestionsAdapter(new ExampleAdapter(this, cursor, items));
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        SearchView searchView =
                (SearchView) menu.findItem(R.id.action_search).getActionView();

        MenuItem shareItem = menu.findItem(R.id.action_share);
        if (TextUtils.isEmpty(searchView.getQuery())) {
            Log.d(TAG, "onPrepareOptionsMenu sw is empty");
            shareItem.setVisible(false);
            return true;
        }

        shareItem.setVisible(true);

        ShareActionProvider myShareActionProvider =
                (ShareActionProvider) MenuItemCompat.getActionProvider(shareItem);

        if (myShareActionProvider != null) {
            Intent shareIntent = new Intent();
            shareIntent.setAction(Intent.ACTION_SEND);
            shareIntent.putExtra(Intent.EXTRA_TEXT, "Hello World!");
            shareIntent.setType("text/plain");
            myShareActionProvider.setShareIntent(shareIntent);
        }
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Log.d(TAG, "Menu click!");
        switch (item.getItemId()) {
            case R.id.action_search:

                return true;
            case R.id.action_share:
                Log.d(TAG, "Action share");
                return true;
            case R.id.help:
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }

    @Override
    protected void onNewIntent(Intent intent) {
        Log.d(TAG, "New Intent!");
        if (ContactsContract.Intents.SEARCH_SUGGESTION_CLICKED.equals(intent.getAction())) {
            //handles suggestion clicked query
            String displayName = getDisplayNameForContact(intent);
            tvResult.setText(displayName);
        } else if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            // handles a search query
            String query = intent.getStringExtra(SearchManager.QUERY);
            tvResult.setText("should search for query: '" + query + "'...");
        }
    }

    private String getDisplayNameForContact(Intent intent) {
       /* Cursor phoneCursor = getContentResolver().query(intent.getData(), null, null, null, null);
        phoneCursor.moveToFirst();
        int idDisplayName = phoneCursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME);
        String name = phoneCursor.getString(idDisplayName);
        phoneCursor.close();*/
        return "blabla";
    }
}
