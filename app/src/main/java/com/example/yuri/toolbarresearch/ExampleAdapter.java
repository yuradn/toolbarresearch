package com.example.yuri.toolbarresearch;

import android.content.Context;
import android.database.Cursor;
import android.support.v4.widget.CursorAdapter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

/**
 * Created by test on 5/23/16.
 */
public class ExampleAdapter extends CursorAdapter {

    private static final String TAG = "ExampleAdapter";
    private List<String> items;

    public ExampleAdapter(Context context, Cursor cursor, List<String> items) {
        super(context, cursor, false);
        this.items = items;
    }

    @Override
    public void bindView(View view, Context context, final Cursor cursor) {
        TextView tv = (TextView) view.findViewById(R.id.item);
        tv.setText(cursor.getString(1));
        tv.setTag(""+cursor.getPosition());
        tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "onClick: " + v.getTag().toString());
            }
        });
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View view = inflater.inflate(R.layout.item, parent, false);

        //text = (TextView) view.findViewById(R.id.text);

        return view;

    }

}
